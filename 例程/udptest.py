from socket import *
from tkinter import *
import time
class Robot():
  def __init__(self):
    self.bufsize = 1024  #定义缓冲大小
    self.addr_A = ('192.168.12.187',10000) # 元祖形式的电机地址
    self.addr_B = ('192.168.12.152',10000)
    self.addr_C = ('192.168.12.33',10000)
    self.udp = socket(AF_INET,SOCK_DGRAM) #创建客户端
    self.udp.settimeout(1)
    self.programe_A = None #存储程序
    self.programe_B = None
    self.programe_C = None
    self.info_a = None
    self.info_b = None
    self.info_c = None
    self.position_a = None #电机位置静态变量
    self.position_b = None
    self.position_c = None
    self.tk = Tk() #创建窗口
    self.position_A = StringVar() #电机位置动态变量
    self.position_B = StringVar()
    self.position_C = StringVar()
    self.A_scale = None #划条属性，用于使用get方法得到目标位置,返回数据是int型
    self.B_scale = None
    self.C_scale = None
    self.sample_scale = None 
    self.speed=IntVar() #步进电机运动速度
    self.programe_window = None #程序编辑框
    self.cursor = 1
  def close_udp(self):
    self.udp.close()
  def reset(self):
    command = 'reset'.encode(encoding="utf-8") 
    self.udp.sendto(command,self.addr_A) # 发送数据
    self.info_a,self.addr_A = self.udp.recvfrom(self.bufsize) #接收数据和返回地址
    self.udp.sendto(command,self.addr_B) # 发送数据
    self.info_b,self.addr_A = self.udp.recvfrom(self.bufsize) #接收数据和返回地址
    self.udp.sendto(command,self.addr_C) # 发送数据
    self.info_c,self.addr_A = self.udp.recvfrom(self.bufsize) #接收数据和返回地址
    self.update_position()
  def get_position(self):
    command = 'now_position'.encode(encoding="utf-8") 
    self.udp.sendto(command,self.addr_A) # 发送数据
    self.position_a,self.addr_A = self.udp.recvfrom(self.bufsize) #接收数据和返回地址
    self.udp.sendto(command,self.addr_B) # 发送数据
    self.position_b,self.addr_B = self.udp.recvfrom(self.bufsize) #接收数据和返回地址
    self.udp.sendto(command,self.addr_C) # 发送数据
    self.position_c,self.addr_C = self.udp.recvfrom(self.bufsize) #接收数据和返回地址
  def update_position(self):
    self.get_position()
    self.position_A.set(self.position_a.decode(encoding="utf-8"))
    self.position_B.set(self.position_b.decode(encoding="utf-8"))
    self.position_C.set(self.position_c.decode(encoding="utf-8"))
  def A_goto(self):
    command = ('goto,'+str(self.A_scale.get())+','+str(self.speed.get())).encode(encoding="utf-8")
    self.udp.sendto(command,self.addr_A) # 发送goto指令和数据
    self.position_A.set(str(self.A_scale.get()))
  def B_goto(self):
    command = ('goto,'+str(self.B_scale.get())+','+str(self.speed.get())).encode(encoding="utf-8")
    self.udp.sendto(command,self.addr_B) # 发送goto指令和数据
    self.position_B.set(str(self.B_scale.get()))
  def C_goto(self):
    command = ('goto,'+str(self.C_scale.get())+','+str(self.speed.get())).encode(encoding="utf-8")
    self.udp.sendto(command,self.addr_C) # 发送goto指令和数据
    self.position_C.set(str(self.C_scale.get()))
  def goto_all(self):
    self.A_goto()
    self.B_goto()
    self.C_goto()
  def A_get_power(self):
    command = 'get_power'.encode(encoding="utf-8")
    self.udp.sendto(command,self.addr_A)
  def B_get_power(self):
    command = 'get_power'.encode(encoding="utf-8")
    self.udp.sendto(command,self.addr_B)
  def C_get_power(self):
    command = 'get_power'.encode(encoding="utf-8")
    self.udp.sendto(command,self.addr_C)
  def get_power(self):
    self.A_get_power()
    self.B_get_power()
    self.C_get_power()
  def A_cut_power(self):
    command = 'cut_power'.encode(encoding="utf-8")
    self.udp.sendto(command,self.addr_A)
  def B_cut_power(self):
    command = 'cut_power'.encode(encoding="utf-8")
    self.udp.sendto(command,self.addr_B)
  def C_cut_power(self):
    command = 'cut_power'.encode(encoding="utf-8")
    self.udp.sendto(command,self.addr_C)
  def cut_power(self):
    self.A_cut_power()
    self.B_cut_power()
    self.C_cut_power()
  def set_sample(self):
    command = ('set_sample,'+str(self.sample_scale.get())).encode(encoding='utf-8')
    self.udp.sendto(command,self.addr_A)
    self.udp.sendto(command,self.addr_B)
    self.udp.sendto(command,self.addr_C)
  def get_programe(self):
    command = 'updata_programe'.encode(encoding="utf-8")
    self.udp.sendto(command,self.addr_A)    
    self.programe_A,self.addr_A = self.udp.recvfrom(self.bufsize)
    print(self.programe_A)
  def put_programe(self):
    pass
  def start_teach(self):
    self.A_cut_power()
    self.B_cut_power()
    self.C_cut_power()
    time.sleep(0.5)
    command = 'start_teach'.encode(encoding="utf-8")
    self.udp.sendto(command,self.addr_A)
    self.udp.sendto(command,self.addr_B)
    self.udp.sendto(command,self.addr_C)
  def stop_teach(self):
    command = 'stop_teach'.encode(encoding="utf-8")
    self.udp.sendto(command,self.addr_A)
    self.udp.sendto(command,self.addr_B)
    self.udp.sendto(command,self.addr_C)
  def run(self):
    self.A_get_power()
    self.B_get_power()
    self.C_get_power()
    time.sleep(0.5)
    command = 'run'.encode(encoding="utf-8")
    self.udp.sendto(command,self.addr_A)
    self.udp.sendto(command,self.addr_B)
    self.udp.sendto(command,self.addr_C)
  def get_position(self):
    command = 'get_position'.encode(encoding="utf-8")
    self.udp.sendto(command,self.addr_A)
    self.position_a,self.addr_A = self.udp.recvfrom(self.bufsize)
    self.udp.sendto(command,self.addr_B)
    self.position_b,self.addr_B = self.udp.recvfrom(self.bufsize)
    self.udp.sendto(command,self.addr_C)
    self.position_c,self.addr_C = self.udp.recvfrom(self.bufsize)
    all_pos = str(self.position_a)[2:-1]+','+str(self.position_b)[2:-1]+','+str(self.position_c)[2:-1]
    self.programe_window.insert(END,all_pos)
    self.programe_window.insert(END,'\n')
  def let_it_go(self):
    self.get_power()
    time.sleep(1)
    text = str(self.programe_window.get('1.0','end')).split('\n')[:-2]
    for i in text:
      pos = i.split(',')
      command_A = ('goto,'+str(pos[0])+','+str(self.speed.get())).encode(encoding="utf-8")
      command_B = ('goto,'+str(pos[1])+','+str(self.speed.get())).encode(encoding="utf-8")
      command_C = ('goto,'+str(pos[2])+','+str(self.speed.get())).encode(encoding="utf-8")
      self.udp.sendto(command_A,self.addr_A) # 发送goto指令和数据
      self.udp.sendto(command_B,self.addr_B) # 发送goto指令和数据
      self.udp.sendto(command_C,self.addr_C) # 发送goto指令和数据
      time.sleep(2.5)
  def interface(self):
    self.tk.title('机器人示教终端')
    self.tk.geometry('1024x600')
    self.tk.resizable(width=False,height=False)
    self.position_A.set('500')
    self.position_B.set('500')
    self.position_C.set('500')
    #运动执行模块
    goto_frame = Frame(self.tk,height=200,width=10240)
    goto_frame.place(x=0,y=40)
    goto_text = Label(goto_frame,text='位置控制')
    goto_text.place(x=0,y=0)
    self.A_scale = Scale(goto_frame,from_=200,to=800,resolution=1,length=900,orient=HORIZONTAL)
    self.A_scale.place(x=5,y=20)
    self.B_scale = Scale(goto_frame,from_=200,to=800,resolution=1,length=900,orient=HORIZONTAL)
    self.B_scale.place(x=5,y=60)
    self.C_scale = Scale(goto_frame,from_=200,to=800,resolution=1,length=900,orient=HORIZONTAL)
    self.C_scale.place(x=5,y=100)
    A_goto_button = Button(goto_frame,text='A运动到此位置',command=self.A_goto)
    A_goto_button.place(x=910,y=30)
    B_goto_button = Button(goto_frame,text='B运动到此位置',command=self.B_goto)
    B_goto_button.place(x=910,y=70)
    C_goto_button = Button(goto_frame,text='C运动到此位置',command=self.C_goto)
    C_goto_button.place(x=910,y=110)
    move_all = Button(goto_frame,text='同时移动电机',command=self.goto_all)
    move_all.place(x=910,y=150)
    #速度选择模块
    #speed_label = LabelFrame(self.tk,text='运行速度')
    #speed_label.place(x=10,y=160)
    #SPEED = [("  极  快    ",100),("  稍  快    ",500),("  正  常    ",1000),("  稍  慢    ",2000),("  极  慢    ",5000)]
    self.speed.set(1000)
    #for spd,num in SPEED:
    #   b = Radiobutton(speed_label,text=spd,variable=self.speed,value=num)
    #   b.pack(anchor=W,padx=10)
    #电源管理模块
    power_label = LabelFrame(self.tk,text='电源管理')
    power_label.place(x=800,y=250)
    A_get_power_button = Button(power_label,text='A 电机上电',command=self.A_get_power)
    A_get_power_button.grid(row=0,column=0,padx=8,pady=12+14)    
    B_get_power_button = Button(power_label,text='B 电机上电',command=self.B_get_power)
    B_get_power_button.grid(row=1,column=0,padx=8,pady=12+14)    
    C_get_power_button = Button(power_label,text='C 电机上电',command=self.C_get_power)
    C_get_power_button.grid(row=2,column=0,padx=8,pady=12+14)
    A_cut_power_button = Button(power_label,text='A 电机断电',command=self.A_cut_power)
    A_cut_power_button.grid(row=0,column=1,padx=8,pady=12+14)    
    B_cut_power_button = Button(power_label,text='B 电机断电',command=self.B_cut_power)
    B_cut_power_button.grid(row=1,column=1,padx=8,pady=12+14)    
    C_cut_power_button = Button(power_label,text='C 电机断电',command=self.C_cut_power)
    C_cut_power_button.grid(row=2,column=1,padx=8,pady=12+14)    
    #程序传输
    programe_label = LabelFrame(self.tk,text='程序传输')
    programe_label.place(x=550,y=250)
    get_programe_button = Button(programe_label,text='上传程序',command=self.get_programe)
    get_programe_button.grid(row=0,padx=8,pady=12+8+6)
    test3 = Button(programe_label,text='记录位置',command=self.get_position)
    test3.grid(row=1,padx=8,pady=12+8+6)
    test4 = Button(programe_label,text='执行程序',command=self.let_it_go)
    test4.grid(row=2,padx=8,pady=12+8+6)
    #put_programe_button = Button(programe_label,text='下载程序',command=self.put_programe)
    #put_programe_button.grid(row=1,padx=8,pady=12)
    #末端控制
    pass
    #示教方式
    teach_label = LabelFrame(self.tk,text='拖动示教')
    teach_label.place(x=670,y=250)
    dirction_teach_button = Button(teach_label,text=' 开 始 示 教 ',command=self.start_teach)
    dirction_teach_button.grid(row=0,padx=8,pady=12+14)
    machine_teach_button = Button(teach_label,text=' 停 止 示 教  ',command=self.stop_teach)
    machine_teach_button.grid(row=1,padx=8,pady=12+14)
    run1 = Button(teach_label,text=' 动 作 重 现 ',command=self.run)
    run1.grid(row=2,padx=8,pady=12+14)
    #程序操作
    programe_text = LabelFrame(self.tk,text='程序编辑')
    programe_text.place(x=10,y=250)
    self.programe_window = Text(programe_text,width=70,height=15)
    bar = Scrollbar(programe_text)
    bar.config(command=self.programe_window.yview)
    self.programe_window.config(yscrollcommand=bar.set)
    bar.pack(side=RIGHT,fill=Y)
    self.programe_window.pack(side=LEFT,fill=BOTH,expand=1)
    #采样精度
    #sample_frame = LabelFrame(self.tk,text='采样精度')
    #sample_frame.place(x=500,y=295)
    #self.sample_scale = Scale(sample_frame,from_=50,to=150,resolution=1,length=200,orient=HORIZONTAL)
    #self.sample_scale.grid(row=0,column=0)
    #sample_button = Button(sample_frame,text='设置采样率',command=self.set_sample)
    #sample_button.grid(row=0,column=1)
    #程序测试
    #programe_edit = LabelFrame(self.tk,text='测试工具')
    #programe_edit.place(x=800,y=295)
    #test1 = Button(programe_edit,text='重现示教动作',command=self.run)
    #test1.pack()
    #test2 = Button(programe_edit,text='同时移动电机',command=self.goto_all)
    #test2.pack()
    #test3 = Button(programe_edit,text='得到当前位置',command=self.get_position)
    #test3.pack()
    #test4 = Button(programe_edit,text='上位机发命令',command=self.let_it_go)
    #test4.pack()

    self.tk.mainloop()

robot = Robot()    
robot.interface()
