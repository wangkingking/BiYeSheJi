from machine import ADC
import time

class Adc():
  def __init__(self):
    self.sum = 0
    self.len = 0
    self.adc = ADC(0)
    self.list = []
  def init(self, len):
    self.len = len
    self.sum = 0
    self.list = []
    for i in range(0,self.len):
      self.list.append(self.adc.read())
    for i in self.list:
      self.sum += i
  def filter_read(self):
    #new_data = self.adc.read()
    new_data = self.read_many()
    self.sum -= self.list[0]
    self.list[0:-1] = self.list[1:]
    self.sum += new_data
    self.list[-1] = new_data
    return self.sum//self.len
  def read_many(self):
    list = []
    w = 80
    for i in range(0,w):
      list.append(self.adc.read())
      time.sleep_us(100)
    for a in range(0,20):
      list.remove(max(list))
      list.remove(min(list))
    h = 0
    for q in list:
      h += q
    return int(h/40)
  def read_10(self):
    list = []
    w = 20
    for i in range(0,w):
      list.append(self.adc.read())
      time.sleep_us(10)
    for a in range(0,5):
      list.remove(max(list))
      list.remove(min(list))
    h = 0
    for q in list:
      h += q
    return int(h/10)
  def read_one(self):
    return self.adc.read()

if __name__ == '__main__':
  adc = Adc()
  adc.init(5)
  while True:
    #a = adc.filter_read()
    a = adc.filter_read()
    print(a)
    time.sleep(0.1)







