import time
from machine import Pin     
blue=Pin(21,Pin.OUT)
green=Pin(22,Pin.OUT)
red=Pin(23,Pin.OUT) 
def color_led(r,b,g):
  red.value(r)
  blue.value(b)
  green.value(g)
while True:
  color_led(1,0,0)
  time.sleep(0.5) 
  color_led(0,1,0)
  time.sleep(0.5)
  color_led(0,0,1)
  time.sleep(0.5)

