#hardware platform: FireBeetle-ESP32

from machine import ADC,Pin
import time 
adc1=ADC(Pin(34))               #create ADC object
adc2=ADC(Pin(35))
adc3=ADC(Pin(32))
adc4=ADC(Pin(33))
def read_angle():
  '''
  角度映射
  '''
  angle = adc1.read()
  if angle<270:
    angle = 270
  if angle>3020:
    angle = 3020
  return ((angle-270)*180/2750)
while True:
  #print(str(adc1.read()/100)+','+str(adc2.read()/100)+','+str(adc3.read()/100)+','+str(adc4.read()/100))
  #print(str(adc1.read()))
  print(read_angle())
  time.sleep(0.2)


