# -*- coding:utf-8 -*-
import sys
#import cv2  
import numpy as np 
import pygame
from pygame import gfxdraw
import time
import sys
import random
from pygame.locals import *
from PIL import Image
import tkinter.colorchooser
import tkinter.filedialog
from tkinter.messagebox import *
import shutil
##############配置#############
full_screen = not True                   #是否全屏显示
set_color = True#是否设置背景色
background = (120,120,120)#背景色
screen_clock = 20                   #屏幕刷新率
##############变量#############

##############函数#############
def get_random(a,b):
    '''得到一个a和b之间的随机数'''
    if a<=b:
        return random.randint(a,b)
    else:
        return random.randint(b,a)
def ask_directory():
    '''询问要打开的文件夹'''
    return tkinter.filedialog.askdirectory()
def ask_open_file_name():
    '''询问要打开的文件'''
    return tkinter.filedialog.askopenfilename()
def test_messagebox():
    a=askokcancel('Sure?','you are beautiful?')
    a=askquestion('Sure?','you are beautiful?')
    a=askretrycancel('Sure?','you are beautiful?')
    a=askyesno('Sure?','you are beautiful?')
    a=askyesnocancel('Sure?','you are beautiful?')
    a=showerror('Sure?','you are beautiful?')
    a=showinfo('Sure?','you are beautiful?')
    a=showwarning('Sure?','you are beautiful?')
def choose_color():
    '''颜色选择函数，返回颜色元组'''
    return tkinter.colorchooser.askcolor()[0]
def get_resolution():
    '''
    得到当前显示器的分辨率,返回长宽信息,触屏:1024x600
    '''
    return pygame.display.list_modes()[0]
def initialize_system():
    global background
    global screen
    screen.blit(img_connect_arm, (0,0))
    pygame.display.flip()
    pass
    time.sleep(1)
    screen.blit(img_check_network, (0,0))
    pygame.display.flip()
    pass
    time.sleep(1)
    screen.blit(img_check_all, (0,0))
    pygame.display.flip()
    pass
    time.sleep(1)
    if set_color:
        background = choose_color()
    screen.fill(background)
    screen.blit(img_interface, (0,0))
    pygame.display.flip()
    
print(get_random(1,10))
pygame.init()
clock = pygame.time.Clock()#配置屏幕刷新率实例
pygame.display.set_caption('机器人示教器')#设置标题信息
width, height = get_resolution()#得到当前屏幕的分辨率信息
height -= 33
if full_screen == True:#实例化一个屏幕显示对象
    screen = pygame.display.set_mode((width, height ), FULLSCREEN | HWSURFACE)#全屏显示
else:
    screen = pygame.display.set_mode((width, height ))#满屏显示,带有标题栏
#界面图片的加载和实例化
img_connect_arm = pygame.image.load('file/正在连接机械臂.png')#加载一张图片
img_check_network = pygame.image.load('file/正在检查网络.png')
img_check_all = pygame.image.load('file/检查所有信息.png')
img_interface = pygame.image.load('file/界面demo.png')
font_45 = pygame.font.Font(None, 30)
clock.tick(screen_clock)
initialize_system()
while True:
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_q:
                sys.exit()
        if event.type == pygame.MOUSEMOTION and event.buttons == (1,0,0):
            xy = event.pos
            all_event = font_45.render(str(xy),True,(0,0,0))
            pygame.draw.rect(screen, background, [50,50,100,25])
            screen.blit(all_event,(50,50))
            gfxdraw.pixel(screen,xy[0],xy[1],(0,0,255))
    pygame.display.flip()
    clock.tick(screen_clock)
    
